


// ES6 Updates



// Exponent Operator

//old
const oldNum = Math.pow(8,2)
console.log(oldNum)

//new
const newNum = 8 ** 2
console.log(newNum)



// Template Literals

/*
- Allows us to write strings without using the concatenate operator(+)

*/


let studentName = "Roland"

// Pre-Template Literal String
console.log("Hello " + studentName + "! Welcome to programming.")

//Template Literal String
console.log(`Hello ${studentName}! Welcome to programming.`)


// Multi-line template literal

const message = `
${studentName} attended a math competition. 
He won it by solving the problem 8 ** 2 with the solution of ${newNum}.
`
console.log(message)


const interestRate = .1
const principal = 1000

console.log(`The interest rate on your savings account is: ${principal * interestRate}`)




//Array Destructuring

console.log("Array Destructuring: ")

const fullName = ['Jeru','Nebur','Palma']

//Pre-Array Destructuring

console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])

//Array Destructuring
const[firstName,middleName,lastName] = fullName

console.log(firstName)
console.log(middleName)
console.log(lastName)


//Object Destructuring

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

//Pre Object Destructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}`)

//Object Destructuring
const {givenName, maidenName, familyName} = person

console.log(givenName)
console.log(maidenName)
console.log(familyName)
console.log(`Hello ${givenName} ${maidenName} ${familyName}`)


// Arrow Functions
const hello = () => {
	return "Goodmorning Batch 241"
}

const printFullName = (firstN,middleI,lastN) => {
	console.log(`${firstN} ${middleI} ${lastN}`)
}

printFullName(`John`, `D`,`Smith`)



const students = ['John', 'Jane', 'Smith']

// Arrow function with loops


students.forEach((student) => {
	console.log(`${student} is a student`)
})


// Implicit Return Statement
const substract = (x,y) => x+y
let difference = substract(3,1)
console.log(difference)


//Default argument value

const greet = (name = 'User') => {
	return `Goodmorning, ${name}`
}

console.log(greet())

//Class-based object blueprints

class Car {
	constructor(brand,name,year){
		this.brand = brand
		this.name = name 
		this.year = year
	}
}

let myCar= new Car()

console.log(myCar)


myCar.brand = "Ford"
myCar.name = "Ranger Raptor"
myCar.year = 2021

console.log(myCar)


//Instantiate a new Object

const myNewCar = new Car("Toyota","Vios",2021)
console.log(myNewCar)

