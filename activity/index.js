//Mini-Activity

/*
1. Using the ES6 Update, get the cube of 8
2. Print the result on the console with the message: 'The interest rate on your savings acount is ' + result
3. Use the template literal in printing out the message


*/


let getCubeOf8 = 8 ** 3
console.log(`The interest rate on your savings account is: ${getCubeOf8}`)


//Mini-Activity
/*
Destructure the address array
print the values in the console: I Live at 258 Washington Avenue, California, 99011
Use template literals

*/

const address = ["258","Washington Ave NW", "California", "99011"]


const [street,lot,city,postal] = address

console.log(`I live at ${street} ${lot} ${city} ${postal}`)


//Mini-activity
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: '20ft 3 in'
}

const {name,species,weight,measurement} = animal

console.log(`${name} was a ${species}. He weight at ${weight} with a measurement of ${measurement}`)



//Mini-Activity


let numbers = [1,2,3,4,5]

numbers.forEach((number) => {
	console.log(number)
	
})


let result = numbers.reduce((accumulator,currentValue) => accumulator + currentValue,0)

console.log(result)


//Mini-Activity

class dog {
	constructor(name,age,breed){
		this.name = name
		this.age = age
		this.breed = breed
	}
}


let myDog = new dog("Panpan",3,"German Shepherd")
console.log(myDog)

let myNewDog = new dog("Gora",2,"Aspin")
console.log(myNewDog)